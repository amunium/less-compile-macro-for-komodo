# LESS compile macro for Komodo IDE/Edit

Just a quick substitute for the obviously better (but slightly more time consuming and less portable) Grunt task or similar. Just stick this in your .komodotools folder, or [install it globally](http://komodoide.com/resources/install-instructions/#pane-macro).

### Features

Currently the macro supports Sass-style partials beginning with underscore (_). These files will not be compile into files - instead, if such a file is saved, all non-partial files in the same folder will be compiled.

* No support for subfolders thus far.